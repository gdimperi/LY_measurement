#!/usr/bin/env python

import matplotlib.pyplot as plt
import os
import math
import numpy as np
import re
import peakutils
import sys
import argparse

from scipy import optimize
from scipy import signal
from scipy.special import factorial
from numpy import sqrt, pi, exp, linspace, loadtxt, log

outdir = "./"

parser = argparse.ArgumentParser()
parser.add_argument("-i","--inputfilie", type=str, dest="inputfile", help="input file", default="1 inch NaI 1# sample- Cs137.Spe" )
parser.add_argument("-o","--outdir", type=str, dest="outdir", help="output dir", default="./" )
parser.add_argument("--plot", action="store_true", dest="plotonly", help="True if plot only", default=False )
args = parser.parse_args()

filename = args.inputfile
outdir = args.outdir

## variable parameters
#filename = "1 inch NaI 1# sample- Cs137.Spe"
#if (len(sys.argv) > 1):
#    filename = sys.argv[1]

eff = 1
ENF = 1.4
Egamma1 = 0.
Egamma2 = 0.661

# get the source 
cs137 = re.search("(C|c)s137", filename)
na22 = re.search("(N|n)a22", filename)
co60 = re.search("(C|c)o60",filename)

if cs137:
    source = "Cs137"
if na22:
    source = "Na22"
if co60:
    source = "Co60"


if na22:
    Egamma1 = 0.511
    Egamma2 = 1.2745
elif co60:
    Egamma1 = 1.173
    Egamma2 = 1.332


# constants
m      = 0.511                    
Ec1     = Egamma1/(1.+2.*Egamma1/m)
Ec2     = Egamma2/(1.+2.*Egamma2/m)

Egamma = Egamma2
Ec = Ec2
if co60 or na22:
    Ec=Egamma1

### Exp
def myexp(x, a, b):
    return exp(a+b*x)

# Gauss: p0 = normalization
#        p1 = mean
#        p2 = sigma
def gauss(x, p0, p1, p2):
    N = p0
    mu = p1
    sigma = p2
    if p2==0:
        print("****** problem **********")
    return N*exp(-0.5*((x-mu)/sigma)**2)

# GaussLY: a single gauss LY peak
def gausslyfp(x, C, c, LY, sigma, A, B):
    #s = sqrt(sigma**2+c**2*LY*Egamma)
    s = sqrt(sigma**2+c**2*(ENF)*LY*Egamma)
    background = 0.
    #if na22 or co60:
    background = exp(A + B*x)
    return gauss(x, C, c*LY*Egamma, s) + exp(A + B*x)

def fermiDirac(x, N, x_F, T):
    return N/(1+exp((x-x_F)/T))

def gausslybs(x, C, c, LY, sigma, NCompton, x_F, T,A,B):
    #s = sqrt(sigma**2+c**2*LY*Ec)
    if(sigma**2+c**2*(ENF)*LY*Ec>0):
        s = sqrt(sigma**2+c**2*(ENF)*LY*Ec)
    else:
        s = 1e-8
    return gauss(x, C, c*LY*Ec, s) + fermiDirac(x, NCompton, x_F, T)  + exp(A + B*x) 


# DoubleGaussLY: double peak
def doubleGaussly(x, C1, C2, c1, c2, LY, sigma1, sigma2, NCompton, x_F, T):
#def doubleGaussly(x, C1, C2, c1, c2, LY, sigma1, NCompton, x_F, T):
    #s1 = sqrt(sigma1**2+c1**2*LY*Ec)
    #s2 = sqrt(sigma2**2+c2**2*LY*Egamma)
    s1 = sqrt(sigma1**2+c1**2*(ENF)*LY*Ec)
    s2 = sqrt(sigma2**2+c2**2*(ENF)*LY*Egamma)
    #FIXME
    #s2=s1
    return gauss(x, C1, c1*LY*Ec, s1) + fermiDirac(x, NCompton, x_F, T) + gauss(x, C2, c2*LY*Egamma, s2)

# DoubleGaussLY_plusExp: double peak plus exp
def doubleGaussPlusExply(x, C1, C2, c1, c2, LY, sigma1, sigma2, A, B):
#def doubleGaussPlusExply(x, C1, C2, c1, c2, LY, sigma1, A, B):
    #if (sigma1**2+c1**2*LY*Ec < 0):
    if (sigma1**2+c1**2*(ENF)*LY*Ec < 0):
        s1=1e-8
    else:
        #s1 = sqrt(sigma1**2+c1**2*LY*Ec)
        s1 = sqrt(sigma1**2+c1**2*(ENF)*LY*Ec)
    #if sigma2**2+c2**2*LY*Egamma<0:
    if sigma2**2+c2**2*(ENF)*LY*Egamma<0:
        s2=1e-8
    else:
        #s2 = sqrt(sigma2**2+c2**2*LY*Egamma)
        s2 = sqrt(sigma2**2+c2**2*(ENF)*LY*Egamma)
    
    ##FIXME
    #s2=s1
    return gauss(x, C1, c1*LY*Egamma1, s1)  + gauss(x, C2, c2*LY*Egamma2, s2) + exp(A +B*x)   

# read data file
print("====================================================================");
print("Reading data file : " + filename)
if cs137:
    print("Egamma            : ", Egamma, " MeV")
    print("Backscattering    : ", Ec, " MeV")
else:
    print("Egamma1            : ", Ec, " MeV")
    print("Egamma2            : ", Egamma, " MeV")
print("====================================================================\n");
f = open(filename, 'r')
lines = f.readlines()

xch  = np.arange(2048)
data = np.zeros(2048)

# drop useless rows
i = 0
max = 0
count = 0

for line in lines:
    if line.startswith(' '):
        x = re.sub(" +", "", line);
        x = re.sub("\n", "", x);
        data[i] = math.floor(float(x))
#debug
#        print(data[i])
        count += data[i]
        i = i+1
        if (data[i - 1] > count/i*0.01) :
            max = i - 1;


print("Spectrum got up to ", max)

# function to fill an array with data
def fill(m, M, data):
    integral = 0.
    xx = np.zeros(M - m)
    yy = np.zeros(M - m)
    for i in range(0, M - m):
        xx[i] = float(i + m)
        yy[i] = float(data[i + m])
        integral += yy[i]
    return xx, yy, integral

# function to fill an array with sqrt(n)
def errors(y):
    sy = np.zeros(len(y))
    for i in range(0, len(y)):
        sy[i] = sqrt(y[i])
        if (sy[i] == 0):
            sy[i] = 1.
    return sy

# prepare arrays of data
xx, yy, integral = fill( 0, max, data)

if not args.plotonly:
    # find peaks
    #if not co60:
    #    peakind = peakutils.indexes(yy, thres = .1, min_dist = math.floor(max/5))
    #else:
    #    peakind = peakutils.indexes(yy, thres = .5, min_dist = math.floor(max/20))
    # find peaks SICCAS
    if not co60:
        peakind = peakutils.indexes(yy, thres = .1, min_dist = math.floor(max/4))
    else:
        peakind = peakutils.indexes(yy, thres = .05, min_dist = math.floor(max/6))
            
    
    nl = len(peakind)
    if (nl >= 1):
        print("Peak finding: ", peakind)
        ifp = peakind[nl - 1]
        ibs = peakind[nl - 2]
    else:
        print("Cannot find peaks")
        exit(-1)

def sigmaEstimate(y, p, base):
    j = p
    peakHeight = y[p] - base
    while (y[j] > 0.5*peakHeight + base):
        j+=1
    return j-p

# define the ROI
x,  y, integral = fill(0, max, data)
fpsigma = errors(y)

def computeChi2(x,y,func,sigma2,res,ndata):
    # compute chi2
    chi2 = 0.
    for i in range(0, ndata):
        chi2 += ((y[i]-func(x[i], *res))/sigma2[i])**2

    chi2r = chi2/(ndata-len(res))
    return chi2, chi2r

# function to extract data in the region of interest
def roi1(x, y, x1m, x1M):
    l = x1M - x1m 
    xx = np.zeros(l)
    yy = np.zeros(l)
    j = 0
    for i in range(0, len(x)):
        if ((x[i] > x1m) and (x[i] <= x1M)):
            xx[j] = x[i]
            yy[j] = y[i]
            j += 1
    return xx, yy

def roi(x, y, x1m, x1M, x2m, x2M):
    l = x1M - x1m + x2M - x2m 
    xx = np.zeros(l)
    yy = np.zeros(l)
    j = 0
    for i in range(0, len(x)):
        if ((x[i] > x1m) and (x[i] <= x1M)):
            xx[j] = x[i]
            yy[j] = y[i]
            j += 1
        if ((x[i] > x2m) and (x[i] <= x2M)):
            xx[j] = x[i]
            yy[j] = y[i]
            j += 1
    return xx, yy



def photoPeakFitting():
    print("+----- starting photoPeakFittin() ")
    # first estimates
    print("\n--------------- ESTIMATES --------------------------------------------")
    cal = (ifp-ibs)/(Egamma-Ec)
    beta = ifp-cal*Egamma
    print("Calibration constant : ", cal, " ADC Ch/MeV")
    backscatter = Ec*cal
    print("Photopeak at         : ", Egamma*cal+beta)
    print("Backscatter peak at  : ", backscatter+beta)
    
# evaluate some parameters for the fit
    fpN = y[ifp]
    fpm = ifp
    fps = sigmaEstimate(y, ifp, 0)
    
    print("Estimated width      : ", fps)

# define a  ROI
    if cs137:
        startxfp = fpm - math.floor(2*fps)
        endxfp = fpm + 2*fps
    elif na22:
        startxfp = fpm - math.floor(2* fps)
        endxfp = fpm + math.floor(2*fps)
    else:
        startxfp = fpm - math.floor(fps)
        endxfp = fpm + math.floor(fps)


    print("*** fitting photopeak only from ", startxfp, " to ", endxfp) 
    
# estimate LY
    lyest = (ifp/fps)**2*(ENF)/Egamma
    print("LY estimate          : ", lyest, " pe/MeV")
    c = ifp/(lyest*Egamma)
    print("calibration estimate : ", c, " ch/pe")
    #if (fps**2-c**2*lyest*Egamma >0):
    #    sigma0 = sqrt(fps**2-c**2*lyest*Egamma)
    if (fps**2-c**2*(ENF)*lyest*Egamma >0):
        sigma0 = sqrt(fps**2-c**2*(ENF)*lyest*Egamma)
    else :
        sigma0 = 1e-8
    print("electr. noise est.   : ", sigma0, " ch")

# estimate the exponential parameters
    print(fps)
    if co60:
        B = (log(y[ifp+4*fps])-log(y[ifp-5*fps]))/(9*fps)
        A = log(y[ifp-5*fps]) - B*x[ifp-5*fps]
    else:
        B = (log(y[ifp+2*fps])-log(y[ifp-3*fps]))/(5*fps)
        A = log(y[ifp-3*fps]) - B*x[ifp-3*fps]
    

    # ---------------------------------------
    # fit photopeak 
    # ---------------------------------------

    print("\n--------------- FIT --------------------------------------------")
    #fp = [y[ifp], c, lyest, sigma0, 0, 0]
    #param_bounds=((0.5*y[ifp],0.5*c,0.5*lyest,-1,-np.inf, -np.inf),(y[ifp]+5*y[ifp],5*c,5*lyest,np.inf,5*A,0))
    fp = [y[ifp], c, lyest, sigma0, A, B]
    fp, fpcov = optimize.curve_fit(gausslyfp, x[startxfp:endxfp], y[startxfp:endxfp], fp, fpsigma[startxfp:endxfp], True)
    param_bounds=((fp[0]-0.5*abs(fp[0]),fp[1]-0.5*abs(fp[1]),fp[2]-0.5*abs(fp[2]),fp[3]-0.5*abs(fp[3])-1,fp[4]-0.5*abs(fp[4]),fp[5]-0.5*abs(fp[5])),(fp[0]+5*abs(fp[0]),fp[1]+5*abs(fp[1]),fp[2]+5*abs(fp[2]),fp[3]+5*abs(fp[3]+1),fp[4]+5*abs(fp[4]),fp[5]+5*abs(fp[5])))
    print(fp)
    print(param_bounds)
    for i in range(0,10):
        fp, fpcov = optimize.curve_fit(gausslyfp, x[startxfp:endxfp], y[startxfp:endxfp], fp, fpsigma[startxfp:endxfp], True, method="trf", maxfev=40000, bounds=param_bounds)
        param_bounds=((fp[0]-0.5*abs(fp[0]),fp[1]-0.5*abs(fp[1]),fp[2]-0.5*abs(fp[2]),fp[3]-0.5*abs(fp[3]),fp[4]-0.5*abs(fp[4]),fp[5]-0.5*abs(fp[5])),(fp[0]+5*abs(fp[0]),fp[1]+5*abs(fp[1]),fp[2]+5*abs(fp[2]),fp[3]+5*abs(fp[3]),fp[4]+5*abs(fp[4]),fp[5]+5*abs(fp[5])))

    print(fp)
    print("+----- end of photoPeakFittin() ")
    
    x_fp, y_fp = roi1(x, y, startxfp, endxfp)
    sigma_fp = errors(y_fp)
    ndata = (endxfp-startxfp)
    chi2_fp, chi2r_fp = computeChi2(x_fp,y_fp,gausslyfp,sigma_fp,fp,ndata)
    print("ndata = ",ndata)
    print("chi2 = ",chi2_fp)
    print("chi2/ndf = ",chi2r_fp)

    return startxfp, endxfp, fps, fp, chi2_fp, chi2r_fp


if not args.plotonly:
    startxfp, endxfp, fps, fp, chi2_fp, chi2r_fp = photoPeakFitting()


    c = fp[1]
    lyest = fp[2]
    
    if na22 :
        Egamma = Egamma1
        Ec = Ec1
        ifp = ibs
        ibs = math.floor(ifp*0.5)
        startxbs, endxbs, fps1, fp1, chi2_fp1, chi2r_fp1 = photoPeakFitting()
    elif  co60:
        Egamma = Egamma1
        Ec = 0
        ifp = ibs
        ibs = 0
        startxbs, endxbs, fps1, fp1, chi2_fp1, chi2r_fp1 = photoPeakFitting()
    
    electronic_sigma = 10
    smearing = 1.15

# ---------------------------------------
# backscattering 
# ---------------------------------------
def backScatteringFitting():
    print("+----- starting backScatteringFit() ")
# define a narrower ROI
    startxbs = math.floor(ibs - fps)
    endxbs = math.floor((ifp + ibs)*0.5 + 3*fps)
    print("*** fitting lower energy peak + Compton from ", startxbs, " to ", endxbs) 
    
# estimate background level
    C = y[ibs] - y[startxbs]
    B = (log(y[ibs+5*fps])-log(y[ibs]))/(5*fps)
    A = log(y[ibs]) - B*x[ibs+5*fps] - y[ibs+5*fps]
    print("Normalization bspeak : ", C)
    print("Compton edge estimate: ", endxbs)
    print("Peak expected at     : ", c*smearing*lyest*Ec)

    #baseline = y[math.floor((ifp + ibs)*0.5)]
    sl = math.floor(fps*0.5)
    print("Lower peak width     : ", sl)

# fit the backscattering/lower energy peak
    #bs = [C, c*smearing, lyest, electronic_sigma, y[startxbs], endxbs, sl]
    bs = [C, c*smearing, lyest, math.floor(fps*0.5), y[ibs + 4*fps], endxbs, sl,A,B]
    bs, fpcov = optimize.curve_fit(gausslybs, x[startxbs:endxbs], y[startxbs:endxbs], bs, fpsigma[startxbs:endxbs], True, maxfev=20000)
    param_bounds = ((bs[0]-0.5*abs(bs[0]),bs[1]-0.5*abs(bs[1]),bs[2]-0.5*abs(bs[2]),bs[3]-0.5*abs(bs[3]),bs[4]-0.5*abs(bs[4]),bs[5]-0.5*abs(bs[5]),bs[6]-0.5*abs(bs[6]),bs[7]-0.5*abs(bs[7]),bs[8]-0.5*abs(bs[8])),(bs[0]+5*abs(bs[0]),bs[1]+5*abs(bs[1]),bs[2]+5*abs(bs[2]),bs[3]+5*abs(bs[3]),bs[4]+5*abs(bs[4]),bs[5]+5*abs(bs[5]),bs[6]+5*abs(bs[6]),bs[7]+5*abs(bs[7]),bs[8]+5*abs(bs[8])))
    #param_bounds = ((0.5*C,0.5*c,0.5*lyest,-1,0,0.5*endxbs,-1,-np.inf,2*B),(np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf,np.inf))
    print(bs)
    print(param_bounds)
    print("====== now doing the fit ========")
    #bs, fpcov = optimize.curve_fit(gausslybs, x[startxbs:endxbs], y[startxbs:endxbs], bs, fpsigma[startxbs:endxbs], True, method="trf",maxfev=40000, bounds=param_bounds)
    for i in range(0,10):
        bs, fpcov = optimize.curve_fit(gausslybs, x[startxbs:endxbs], y[startxbs:endxbs], bs, fpsigma[startxbs:endxbs], True, method="trf",maxfev=40000, bounds=param_bounds)
        param_bounds = ((bs[0]-0.5*abs(bs[0]),bs[1]-0.5*abs(bs[1]),bs[2]-0.5*abs(bs[2]),bs[3]-0.5*abs(bs[3]),bs[4]-0.5*abs(bs[4]),bs[5]-0.5*abs(bs[5]),bs[6]-0.5*abs(bs[6]),bs[7]-0.5*abs(bs[7]),bs[8]-0.5*abs(bs[8])),(bs[0]+5*abs(bs[0]),bs[1]+5*abs(bs[1]),bs[2]+5*abs(bs[2]),bs[3]+5*abs(bs[3]),bs[4]+5*abs(bs[4]),bs[5]+5*abs(bs[5]),bs[6]+5*abs(bs[6]),bs[7]+5*abs(bs[7]),bs[8]+5*abs(bs[8])))

    print(bs)
    print("+----- end backScatteringFit() ")
    x_bs, y_bs = roi1(x, y, startxbs, endxbs)
    sigma_bs = errors(y_bs)
    ndata = (endxbs-startxbs)
    chi2_bs, chi2r_bs = computeChi2(x_bs,y_bs,gausslybs,sigma_bs,bs,ndata)
    print("ndata = ",ndata)
    print("chi2 = ",chi2_bs)
    print("chi2/ndf = ",chi2r_bs)

    return startxbs, endxbs, sl, bs, chi2_bs, chi2r_bs


if not args.plotonly:
    if cs137:
        startxbs, endxbs, sl, bs, chi2_bs, chi2r_bs = backScatteringFitting() 


    # ---------------------------------------------------------
    # whole spectrum
    # ---------------------------------------------------------
    print("*** Fitting whole spectrum ***")
    if not co60:
        x2, y2 = roi(x, y, startxbs, endxbs, startxfp, endxfp)
    else:
        x2, y2 = roi(x, y, startxbs-2*fps, endxbs, startxfp, endxfp+2*fps)
    
    sigma2 = errors(y2)
    
    
    print("Fit starts at        : ", startxbs)
    print("Fit stops at         : ", endxfp)
    
    if cs137:
        print("Compton lev. estimate: ", bs[4])
        print("Compton edge estimate: ", bs[5])
        #FIXME
        fullp = [bs[0], fp[0], fp[1]*smearing, fp[1], fp[2], bs[3], fp[3], bs[4], bs[5], bs[6]]
        param_bounds=((0.5*bs[0],0.5*fp[0],0.5*fp[1],0.5*fp[1],0.5*fp[2],-1,-1,0,0,0),(2*bs[0],2*fp[0],2*fp[1],2*fp[1],2*fp[2],fps*10,fps*10,2*bs[4],2*bs[5],2*bs[6]))
        #fullp = [bs[0], fp[0], fp[1]*smearing, fp[1], fp[2], fp[3], bs[4], bs[5], bs[6]]
        #param_bounds=((0.5*bs[0],0.5*fp[0],0.5*fp[1],0.5*fp[1],0.5*fp[2],-1,0,0,0),(2*bs[0],2*fp[0],2*fp[1],2*fp[1],2*fp[2],fps*10,2*bs[4],2*bs[5],2*bs[6]))
        fullp, fpcov = optimize.curve_fit(doubleGaussly, x2, y2, fullp, sigma2, True, method="trf", maxfev=10000)
        print(fullp)
        print(fpcov)
        print("====== now doing the fit ========")
        for i in range(0,10):
            #param_bounds=((fullp[0]-0.5*abs(fullp[0]),fullp[1]-0.5*abs(fullp[1]),fullp[2]-0.5*abs(fullp[2]),fullp[3]-0.5*abs(fullp[3]),fullp[4]-0.5*abs(fullp[4]),fullp[5]-0.5*abs(fullp[5]),fullp[6]-0.5*abs(fullp[6]),fullp[7]-0.5*abs(fullp[7]),fullp[8]-0.5*abs(fullp[8])),(fullp[0]+5*abs(fullp[0]),fullp[1]+5*abs(fullp[1]),fullp[2]+5*abs(fullp[2]),fullp[3]+5*abs(fullp[3]),fullp[4]+5*abs(fullp[4]),fullp[5]+5*abs(fullp[5]),fullp[6]+5*abs(fullp[6]),fullp[7]+5*abs(fullp[7]),fullp[8]+5*abs(fullp[8])))
            param_bounds=((fullp[0]-0.5*abs(fullp[0]),fullp[1]-0.5*abs(fullp[1]),fullp[2]-0.5*abs(fullp[2]),fullp[3]-0.5*abs(fullp[3]),fullp[4]-0.5*abs(fullp[4]),fullp[5]-0.5*abs(fullp[5]),fullp[6]-0.5*abs(fullp[6]),fullp[7]-0.5*abs(fullp[7]),fullp[8]-0.5*abs(fullp[8]),fullp[9]-0.5*abs(fullp[9])),(fullp[0]+5*abs(fullp[0]),fullp[1]+5*abs(fullp[1]),fullp[2]+5*abs(fullp[2]),fullp[3]+5*abs(fullp[3]),fullp[4]+5*abs(fullp[4]),fullp[5]+5*abs(fullp[5]),fullp[6]+5*abs(fullp[6]),fullp[7]+5*abs(fullp[7]),fullp[8]+5*abs(fullp[8]),fullp[9]+5*abs(fullp[9])))
            #param_bounds=((fullp[0]-3*sqrt(fpcov[0][0]),fullp[1]-3*sqrt(fpcov[1][1]),fullp[2]-3*sqrt(fpcov[2][2]),fullp[3]-3*sqrt(fpcov[3][3]),fullp[4]-3*sqrt(fpcov[4][4]),fullp[5]-3*sqrt(fpcov[5][5]),fullp[6]-3*sqrt(fpcov[6][6]),fullp[7]-3*sqrt(fpcov[7][7]),fullp[8]-3*sqrt(fpcov[8][8]),fullp[9]-3*sqrt(fpcov[9][9])),(fullp[0]+3*sqrt(fpcov[0][0]),fullp[1]+3*sqrt(fpcov[1][1]),fullp[2]+3*sqrt(fpcov[2][2]),fullp[3]+3*sqrt(fpcov[3][3]),fullp[4]+3*sqrt(fpcov[4][4]),fullp[5]+3*sqrt(fpcov[5][5]),fullp[6]+3*sqrt(fpcov[6][6]),fullp[7]+3*sqrt(fpcov[7][7]),fullp[8]+3*sqrt(fpcov[8][8]),fullp[9]+3*sqrt(fpcov[9][9])))
            fullp, fpcov = optimize.curve_fit(doubleGaussly, x2, y2, fullp, sigma2, True, method="trf", maxfev=10000, bounds=param_bounds)
        
        fullp, fpcov = optimize.curve_fit(doubleGaussly, x2, y2, fullp, sigma2, True, method="trf", maxfev=10000, bounds=param_bounds)
            
       
        # fit results
        print(fullp)
        #print(fpcov)
    
        fpcov_norm = [[0 for ii in range(0,len(fullp))] for jj in range(0,len(fullp))]
        for ii in range(0, len(fullp)):
            jj=0
            for jj in range(0,len(fullp)):
                fpcov_norm[ii][jj] = fpcov[ii][jj]/sqrt(fpcov[ii][ii]*fpcov[jj][jj])
            print(fpcov_norm[ii])
    
        cFbs = fullp[2]
        cF = fullp[3]
        LYF = fullp[4]
        sFbs = fullp[5]
        sF = fullp[6]
        #sF = fullp[5]
        #FIXME
        #sFbs =sF
    
        photopeak = cF*LYF*Egamma
        print("\nsigma const backscattering = ",sFbs," ch")
        print("Width backscattering = ",sqrt(sFbs**2+cFbs**2*(ENF)*LYF*Ec)," ch\n")
        print("sigma const = ",sF," ADC ch")
        print("Photopeak  = ",photopeak," ch")
        print("Width  = ",sqrt(sF**2+cF**2*(ENF)*LYF*Egamma)," ch\n")
        
        ndata = len(y2)
        chi2_fullp, chi2r_fullp = computeChi2(x2,y2,doubleGaussly,sigma2,fullp,ndata)
        print("chi2 = ",chi2_fullp)
        print("chi2/ndf = ",chi2r_fullp)
        
    else:
        
        ##FIXME
        fullp = [fp1[0], fp[0], fp[1], fp[1], fp[2], fps1, fps, fp1[4], fp1[5]]
        param_bounds=[0.5*fp1[0],0.5*fp[0],0.5*fp[1],0.5*fp[1],0.5*fp[2],-1,-1,fp1[4]-abs(0.5*fp1[4]),2*fp1[5]],[2*fp1[0],2*fp[0],2*fp[1],2*fp[1],2*fp[2],5*fps,5*fps,abs(2*fp1[4]),0]
        #fullp = [fp1[0], fp[0], fp[1], fp[1], fp[2], fps, fp1[4], fp1[5]]
        #param_bounds=[0.5*fp1[0],0.5*fp[0],0.5*fp[1],0.5*fp[1],0.5*fp[2],-1,fp1[4]-abs(0.5*fp1[4]),2*fp1[5]],[2*fp1[0],2*fp[0],2*fp[1],2*fp[1],2*fp[2],5*fps,abs(2*fp1[4]),0]
        print(fullp)
        print(param_bounds)
        print("====== now doing the fit ========")
        fullp, fpcov = optimize.curve_fit(doubleGaussPlusExply, x2, y2, fullp, sigma2, True,method="trf", maxfev=10000) 
        for i in range(0,10):
            #param_bounds=((fullp[0]-0.5*abs(fullp[0]),fullp[1]-0.5*abs(fullp[1]),fullp[2]-0.5*abs(fullp[2]),fullp[3]-0.5*abs(fullp[3]),fullp[4]-0.5*abs(fullp[4]),fullp[5]-0.5*abs(fullp[5]),fullp[6]-0.5*abs(fullp[6]),fullp[7]-0.5*abs(fullp[7])),(fullp[0]+5*abs(fullp[0]),fullp[1]+5*abs(fullp[1]),fullp[2]+5*abs(fullp[2]),fullp[3]+5*abs(fullp[3]),fullp[4]+5*abs(fullp[4]),fullp[5]+5*abs(fullp[5]),fullp[6]+5*abs(fullp[6]),fullp[7]+5*abs(fullp[7])))
            param_bounds=((fullp[0]-0.5*abs(fullp[0]),fullp[1]-0.5*abs(fullp[1]),fullp[2]-0.5*abs(fullp[2]),fullp[3]-0.5*abs(fullp[3]),fullp[4]-0.5*abs(fullp[4]),fullp[5]-0.5*abs(fullp[5]),fullp[6]-0.5*abs(fullp[6]),fullp[7]-0.5*abs(fullp[7]),fullp[8]-0.5*abs(fullp[8])),(fullp[0]+5*abs(fullp[0]),fullp[1]+5*abs(fullp[1]),fullp[2]+5*abs(fullp[2]),fullp[3]+5*abs(fullp[3]),fullp[4]+5*abs(fullp[4]),fullp[5]+5*abs(fullp[5]),fullp[6]+5*abs(fullp[6]),fullp[7]+5*abs(fullp[7]),fullp[8]+5*abs(fullp[8])))
            #param_bounds=((fullp[0]-10*sqrt(fpcov[0][0]),fullp[1]-10*sqrt(fpcov[1][1]),fullp[2]-10*sqrt(fpcov[2][2]),fullp[3]-10*sqrt(fpcov[3][3]),fullp[4]-10*sqrt(fpcov[4][4]),fullp[5]-10*sqrt(fpcov[5][5]),fullp[6]-10*sqrt(fpcov[6][6]),fullp[7]-10*sqrt(fpcov[7][7]),fullp[8]-10*sqrt(fpcov[8][8])),(fullp[0]+10*sqrt(fpcov[0][0]),fullp[1]+10*sqrt(fpcov[1][1]),fullp[2]+10*sqrt(fpcov[2][2]),fullp[3]+10*sqrt(fpcov[3][3]),fullp[4]+10*sqrt(fpcov[4][4]),fullp[5]+10*sqrt(fpcov[5][5]),fullp[6]+10*sqrt(fpcov[6][6]),fullp[7]+10*sqrt(fpcov[7][7]),fullp[8]+10*sqrt(fpcov[8][8])))
            fullp, fpcov = optimize.curve_fit(doubleGaussPlusExply, x2, y2, fullp, sigma2, True,method="trf", maxfev=40000, bounds=param_bounds) 
            
        fullp, fpcov = optimize.curve_fit(doubleGaussPlusExply, x2, y2, fullp, sigma2, True,method="trf", maxfev=10000, bounds=param_bounds) 
    
        # fit results
        print(fullp)
        fpcov_norm = [[0 for ii in range(0,len(fullp))] for jj in range(0,len(fullp))]
        for ii in range(0, len(fullp)):
            jj=0
            for jj in range(0,len(fullp)):
                fpcov_norm[ii][jj] = fpcov[ii][jj]/sqrt(fpcov[ii][ii]*fpcov[jj][jj])
            print(fpcov_norm[ii])
    
    
        ##FIXME
        c1F = fullp[2]
        c2F = fullp[3]
        LYF = fullp[4]
        s1F = fullp[5]
        s2F = fullp[6]
        #s2F = s1F
    
        photopeak1 = c1F*LYF*Egamma1
        photopeak = c2F*LYF*Egamma2
       
        print("\nsigma 1 const = ",s1F," ch    sigma 2 const = ",s2F," ch")
        print("Photopeak 1 = ",photopeak1," ch    Photopeak2 = ",photopeak," ch")
        print("Width 1 = ",sqrt(s1F**2+c1F**2*(ENF)*LYF*Egamma1)," ch    Width 2 = ",sqrt(s2F**2+c2F**2*(ENF)*LYF*Egamma2)," ch\n")
    
        # compute chi2
        ndata = len(y2)
        chi2_fullp, chi2r_fullp = computeChi2(x2,y2,doubleGaussPlusExply,sigma2,fullp,ndata)
        print("chi2 = ",chi2_fullp)
        print("chi2/ndf = ",chi2r_fullp)
    
    
    
    #print("Chi2 = ",chi2, " NDF = ", len(y2)-len(fullp)-1, " Chi2_r = ", chi2/(len(y2)-len(fullp)-1))
    print("======================================================================\n")
    if cs137:
        print("Preliminary fits:")
        print("LY(bs) = ", bs[2], " pe/MeV")
        print("LY(fp) = ", fp[2], " pe/MeV")
        print("photopeak = ", fp[1]*fp[2]*Egamma2, " ADC")
        print("width = ", sqrt(fp[3]**2+fp[1]**2*(ENF)*fp[2]*Egamma2), " ADC")
        print("FWHM (prefit) @ ",Egamma," MeV  = ", 2.35*sqrt(fp[3]**2+fp[1]**2*(ENF)*fp[2]*Egamma)/(fp[1]*fp[2]*Egamma2))
        print("======================================================================\n")
        print("LY     = ", LYF, " +/- ",sqrt(fpcov[4][4])," pe/MeV c_1 = ", fullp[2], " c_2 = ", fullp[3])
        print("FWHM @ ",Egamma," MeV  = ", 2.35*sqrt(sF**2+cF**2*(ENF)*LYF*Egamma)/photopeak)
    else:
        print("Preliminary fits:")
        print("LY(fp1) = ", fp1[2], " pe/MeV")
        print("photopeak1 = ", fp1[1]*fp1[2]*Egamma1, " ADC")
        print("width1 = ", sqrt(fp1[3]**2+fp1[1]**2*(ENF)*fp1[2]*Egamma1), " ADC")
        print("FWHM @ ",Egamma1," MeV = ", 2.35*sqrt(fp1[3]**2+fp1[1]**2*(ENF)*fp1[2]*Egamma1)/(fp1[1]*fp1[2]*Egamma1))
        print("LY(fp2) = ", fp[2], " pe/MeV")
        print("photopeak2 = ", fp[1]*fp[2]*Egamma2, " ADC")
        print("width2 = ", sqrt(fp[3]**2+fp[1]**2*(ENF)*fp[2]*Egamma2), " ADC")
        print("FWHM @ ",Egamma2,"  = ", 2.35*sqrt(fp[3]**2+fp[1]**2*(ENF)*fp[2]*Egamma2)/(fp[1]*fp[2]*Egamma2))
        print("======================================================================\n")
        print("LY     = ", LYF, " +/- ",sqrt(fpcov[4][4])," pe/MeV c_1 = ", fullp[2], " c_2 = ", fullp[3])
        print("FWHM @ ",Egamma1," MeV = ", 2.35*sqrt(s1F**2+c1F**2*(ENF)*LYF*Egamma1)/photopeak1)
        print("FWHM @ ",Egamma2,"  = ", 2.35*sqrt(s2F**2+c2F**2*(ENF)*LYF*Egamma2)/photopeak)
    
    print("======================================================================\n")
    
    fitresults = open(outdir+"/fit_results_"+source+".txt","w") 
    
    fitresults.write("======================================================================\n")
    if cs137:
        fitresults.write("photopeak @ "+str(photopeak)+" ADC channels\n")
        fitresults.write("sigma = "+str(sqrt(sF**2+cF**2*LYF*Egamma))+" ADC channels\n")
        fitresults.write("LY (backscattering fit) = "+str(bs[2])+ " pe/MeV \n")
        fitresults.write("LY (photopeak) = "+ str(fp[2])+ " pe/MeV \n")
        fitresults.write("LY (simultaneous fit)    = "+ str(LYF)+"\n")
        fitresults.write("FWHM @ "+str(Egamma)+" MeV  = "+ str(2.35*sqrt(sF**2+cF**2*(ENF)*LYF*Egamma)/photopeak)+"\n")
        fitresults.write("======================================================================\n")
        fitresults.write("Chi2 (backscattering) = "+ str(chi2_bs)+ "\nChi2_r(backscattering) = "+ str(chi2r_bs)+"\n")
        fitresults.write("======================================================================\n")
        fitresults.write("Chi2 (photopeak) = "+ str(chi2_fp)+ "\nChi2_r(photopeak) = "+ str(chi2r_fp)+"\n")
        fitresults.write("======================================================================\n")
    else:
        fitresults.write("photopeak 1 @ "+str(photopeak1)+" ADC channels\n")
        fitresults.write("sigma 1 = "+str(sqrt(s1F**2+c1F**2*(ENF)*LYF*Egamma1))+" ADC channels\n")
        fitresults.write("photopeak 2 @ "+str(photopeak)+" ADC channels\n")
        fitresults.write("sigma 2 = "+str(sqrt(s2F**2+c2F**2*(ENF)*LYF*Egamma2))+" ADC channels\n")
        fitresults.write("LY (photopeak 1) = "+ str(fp1[2])+ " pe/MeV \n")
        fitresults.write("LY (photopeak 2) = "+ str(fp[2])+ " pe/MeV \n")
        fitresults.write("LY (simultaneous fit) = "+ str(LYF)+ " pe/MeV \n")
        fitresults.write("FWHM @ "+str(Egamma1)+" MeV = "+ str(2.35*sqrt(s1F**2+c1F**2*(ENF)*LYF*Egamma1)/photopeak1) + "\n")
        fitresults.write("FWHM @ "+str(Egamma2)+"  = "+ str(2.35*sqrt(s2F**2+c2F**2*(ENF)*LYF*Egamma2)/photopeak) + "\n")
        fitresults.write("======================================================================\n")
        fitresults.write("Chi2 (photopeak1) = "+ str(chi2_fp1)+ " \nChi2_r(photopeak1) = "+ str(chi2r_fp1)+"\n")
        fitresults.write("======================================================================\n")
        fitresults.write("Chi2 (photopeak2) = "+ str(chi2_fp)+ " \nChi2_r(photopeak2) = "+ str(chi2r_fp)+"\n")
        fitresults.write("======================================================================\n")
    
    fitresults.write("Chi2 (simultaneous) = "+ str(chi2_fullp)+ " \nChi2_r = "+ str(chi2r_fullp)+"\n")
    fitresults.write("======================================================================\n")


# plot results
fig = plt.figure()
fulldata = plt.errorbar(x, y, yerr=fpsigma, xerr=0.5, marker="o", ls="", ms=2, label= "Full spectrum")
if not args.plotonly:
    fitdata = plt.errorbar(x2, y2, yerr=sigma2, xerr=0.5, marker="o", ls="", ms=2, label = "Fitted spectrum")
    plt.plot(x[peakind], y[peakind], marker="o", ls="", ms=5)
    ###
    Egamma = Egamma2
    if cs137:
        photopeak, = plt.plot(x[startxfp:endxfp], gausslyfp(x[startxfp:endxfp], *fp), label = "Fit to photopeak")
        backscatter, = plt.plot(x[startxbs:endxbs], gausslybs(x[startxbs:endxbs], *bs), label = "Fit to backscatter")
        fullplot, = plt.plot(x[startxbs:endxfp], doubleGaussly(x[startxbs:endxfp], *fullp),label="Simultaneous fit")
        plt.legend(handles=[fulldata, fitdata,backscatter,photopeak,fullplot])
    else:
        Egamma = Egamma1
        photopeak1, = plt.plot(x[startxbs:endxbs], gausslyfp(x[startxbs:endxbs], *fp1), label="Fit to low photopeak")
        photopeak2, = plt.plot(x[startxfp:endxfp], gausslyfp(x[startxfp:endxfp], *fp), label = "Fit to high photopeak")
        if na22:
            fullplot, = plt.plot(x[startxbs:endxfp], doubleGaussPlusExply(x[startxbs:endxfp], *fullp), label="Simultaneous fit")
        else:
            fullplot, = plt.plot(x[startxbs-2*fps:endxfp+2*fps], doubleGaussPlusExply(x[startxbs-2*fps:endxfp+2*fps], *fullp), label="Simultaneous fit")
        plt.legend(handles=[fulldata, fitdata,photopeak1,photopeak2,fullplot])
    
plt.xlabel('ADC Counts')
plt.ylabel('Events')

plt.show()

if not args.plotonly:
    if cs137:
        fig.savefig(outdir+"/Fit_Cs137.pdf")
        fig.savefig(outdir+"Fit_Cs137.png")
    elif na22:
        fig.savefig(outdir+"/Fit_Na22.pdf")
        fig.savefig(outdir+"/Fit_Na22.png")
    elif co60:
        fig.savefig(outdir+"/Fit_Co60.pdf")
        fig.savefig(outdir+"/Fit_Co60.png")
    
    
