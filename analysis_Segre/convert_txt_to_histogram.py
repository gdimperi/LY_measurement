from ROOT import *
import argparse


parser = argparse.ArgumentParser(description='')
parser.add_argument('--input', dest='input', type=str,
                    default='', help='input file name')
parser.add_argument('--output', dest='output', type=str,
                    default='', help='output file name')
#parser.add_argument('--histname', dest='histname', type=str,
#                    default='', help='histogram name')
args = parser.parse_args()

#Resistence = 50 ohm
R = 50 #ohm

def invertsign(num):
    num *= -1
    return num


def main():
    nlines = 0
    xbins = []
    y = []
    
    for line_ in open(args.input,'r'):
        if line_.startswith('#time'):
            time = float(line_.split()[1])
    print time 
    
    #time = 1

    in_file = open(args.input,'r')
    for line in reversed(in_file.readlines()):
        if not line.startswith('#'):
            xbins.append(invertsign(float(line.split()[0]))/R*1e9 )
            #try to correct the offset by eye FIXME
            #xbins.append(invertsign(float(line.split()[0]))/R*1e9 - 2.5  )
            y.append(float(line.split()[1])/time)
            nlines += 1

    binwidth = float(xbins[1])-float(xbins[0])

    out_file = TFile(args.output,'recreate')
    out_file.cd()
    hist = TH1F("hist","",nlines,xbins[0],xbins[nlines-1]+binwidth)
    nEvt = 0
    for i in range(0,nlines):
        hist.SetBinContent(i+1,y[i])
        nEvt+=(y[i]*time)
    hist.GetXaxis().SetTitle('nC')
    hist.GetYaxis().SetTitle('Rate [Hz]')
    hist.Write()
    out_file.Close()
    print "N events: %d" % nEvt
    print "Rate: %f" % (float(nEvt)/float(time))

if __name__ == "__main__":
    main()
