from ROOT import *
import argparse

gStyle.SetOptStat(0)


parser = argparse.ArgumentParser(description='')
parser.add_argument('--input', dest='input', type=str,
                    default='', help='input file name')
parser.add_argument('--outdir', dest='outdir', type=str,
                    default='', help='output directory')
args = parser.parse_args()

rootfile_in = TFile(args.input,"read")
h_spectrum = rootfile_in.Get("hist")
h_spectrum_cal = rootfile_in.Get("spectrum_cal")
fit_1 = rootfile_in.Get("func")
fit_2 = rootfile_in.Get("func2")
fit_3 = rootfile_in.Get("func3")
fit_4 = rootfile_in.Get("func4")
line_cal = rootfile_in.Get("line")
line_res = rootfile_in.Get("line2")
fit_pe = rootfile_in.Get("func_pe")


c = TCanvas("c","",700,600)
c.cd()
h_spectrum.Draw("hist")
fit_1.Draw("same")
fit_2.Draw("same")
c.SaveAs(args.outdir+"/spectrum.png")

h_spectrum.Draw("hist")
fit_pe.Draw("same")
c.SaveAs(args.outdir+"/spectrum_fit_pe.png")

h_spectrum_cal.Draw("hist")
fit_3.Draw("same")
fit_4.Draw("same")
c.SaveAs(args.outdir+"/spectrum_cal.png")

calibration.Draw("ape")
line_cal.Draw("same")
calibration.SetMarkerStyle(20)
calibration.SetMarkerSize(0.9)
c.SaveAs(args.outdir+"/calibration.png")

resolution.Draw("ape")
line_res.Draw("same")
resolution.SetMarkerStyle(20)
resolution.SetMarkerSize(0.9)
c.SaveAs(args.outdir+"/resolution.png")

