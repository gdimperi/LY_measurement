from ROOT import *
import argparse
from array import array
import math

parser = argparse.ArgumentParser(description='')
parser.add_argument('--spectrum', dest='spectrum', type=str,
                    default='', help='spectrum file name')
parser.add_argument('--bkg', dest='bkg', type=str,
                    default='', help='background file name')
parser.add_argument('--output', dest='output', type=str,
                    default='', help='output file name')
args = parser.parse_args()


def calibrate(raw,offset,coeff):
    cal = offset + raw*coeff
    return cal


def main():

    spectrum_file = TFile(args.spectrum,'read')
    h_spectrum = spectrum_file.Get('hist')
    h_spectrum.Print()    
    #bkg_file = TFile(args.bkg,'read')
    #h_bkg = bkg_file.Get('hist')
    #
    #h_bkgsub = h_spectrum.Add(h_bkg,-1) 

#    test = TF1("test","[0]+[1]*x",0,1)
#    test.SetParameter(0,0)
#    test.SetParameter(1,1)
#    test.Print()
#    test.Draw()
#
    func = TF1("func","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4])",4,6.5)
##NaI Short
    func.SetParameter(0, 3.21153e-01)  
    func.SetParameter(1, -2.00475e-03) 
    func.SetParameter(2, 2.45043e-01)  
    func.SetParameter(3, 5.11707e+00 ) 
    func.SetParameter(4, 3.51793e-01) 
    func.SetParLimits(0, 0,1)  
    func.SetParLimits(1, -0.1,0.1) 
    func.SetParLimits(2, 0.1,0.4)  
    func.SetParLimits(3, 4.9,5.2) 
    func.SetParLimits(4, 0.34,0.36) 
  
    ##assuming that the 2 lines of Co60 have the same intensity
    func2 = TF1("func2","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4]) + [2]*TMath::Gaus(x,[5],[6])",8,13)
    func2.SetParameter(0, 7.43992e-01)  
    func2.SetParameter(1, -5.66204e-02) 
    func2.SetParameter(2, 4.2e-01)
    func2.SetParameter(3, 9.46848e+00) 
    func2.SetParameter(4, 4.00201e-01) 
    func2.SetParameter(5, 1.07370e+01 ) 
    func2.SetParameter(6, 4.33604e-01) 

    func2.SetParLimits(0, 0,1)  
    func2.SetParLimits(1, -1,0) 
    func2.SetParLimits(2, 0.1,0.5)  
    func2.SetParLimits(3, 9.2,9.8) 
    func2.SetParLimits(4, 0.38,0.42) 
    #func2.SetParLimits(5, 0.1,0.5) 
    func2.SetParLimits(5, 10.4,10.8) 
    func2.SetParLimits(6, 0.43,0.45) 

################### Cristallo di Pirro
#    func = TF1("func","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4])",0.6,2)
#    func.SetParameter(0, 3.78790e-01)  
#    func.SetParameter(1, -6.99356e-02) 
#    func.SetParameter(2, 1)  
#    func.SetParameter(3, 1.3) 
#    func.SetParameter(4, 0.2) 


  
#    ##assuming that the 2 lines of Co60 have the same intensity
#    func2 = TF1("func2","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4]) + [2]*TMath::Gaus(x,[5],[6])",1.8,4.5)
#    func2.SetParameter(0, 9.2e-02)  
#    func2.SetParameter(1, -1.3e-02) 
#    func2.SetParameter(2, 0.2)  
#    func2.SetParameter(3, 2.7) 
#    func2.SetParameter(4, 0.1) 
#    #func2.SetParameter(5, 0.2) 
#    func2.SetParameter(5, 3.1) 
#    func2.SetParameter(6, 0.1) 
#
#    func2.SetParLimits(0, 0,1)  
#    func2.SetParLimits(1, -1,0) 
#    func2.SetParLimits(2, 0.1,0.5)  
#    func2.SetParLimits(3, 2.5,2.8) 
#    func2.SetParLimits(4, 0.05,0.3) 
#    #func2.SetParLimits(5, 0.1,0.5) 
#    func2.SetParLimits(5, 2.8,3.5) 
#    func2.SetParLimits(6, 0.05,0.3) 
#
###############################################

    h_spectrum.Fit("func","LLMR")
    h_spectrum.Fit("func2","LLMR")


    ##Calibration
    x=[func.GetParameter(3),func2.GetParameter(3),func2.GetParameter(5)]
    ex=[func.GetParError(3),func2.GetParError(3),func2.GetParError(5)]
    y=[661.,1173.,1332.]
    ey=[0,0.,0]
    
    vx  = array('d',x )
    vex = array('d',ex)
    vy  = array('d',y )
    vey = array('d',ey)

    graph = TGraphErrors(3,vx,vy,vex,vey)
    graph.SetName("calibration")
    #Cristallo di Pirro
    #line = TF1("line","[0]+[1]*x",4,3.5)
    #line.SetParameter(0,400)
    #line.SetParameter(1,200)
    #NaI short
    line = TF1("line","[0]+[1]*x",4,13)
    line.SetParameter(0,50)
    line.SetParameter(1,120)
    line.SetParLimits(0,0.,100.)
    graph.Fit("line","MLLR")
    graph.GetXaxis().SetTitle("charge [nC]")
    graph.GetYaxis().SetTitle("E [keV]")


    cal_p0 = line.GetParameter(0)
    cal_p1 = line.GetParameter(1)
    bins = h_spectrum.GetNbinsX()
    #xmin = cal_p0 + cal_p1 * h_spectrum.GetXaxis().GetBinLowEdge(1)
    #xmax = cal_p0 + cal_p1 * h_spectrum.GetXaxis().GetBinUpEdge(bins)
    xmin = calibrate(h_spectrum.GetXaxis().GetBinLowEdge(1),cal_p0,cal_p1)
    xmax = calibrate(h_spectrum.GetXaxis().GetBinUpEdge(bins),cal_p0,cal_p1)

    spectrum_cal = TH1F("spectrum_cal","",bins,xmin,xmax)
    for i in range(0,bins):
        spectrum_cal.SetBinContent(i,h_spectrum.GetBinContent(i))

    spectrum_cal.GetXaxis().SetTitle("Energy [keV]")
    spectrum_cal.GetYaxis().SetTitle("Rate [Hz]")


    ##Resolution
    func3 = TF1("func3","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4])",480,840)
    #func3 = TF1("func3","[0]+[1]*x",620,700)
    func3.SetParameter(0, 5.76304e-01)  
    func3.SetParameter(1, -6.33378e-05) 
    func3.SetParameter(2, 0.2)  
    func3.SetParameter(3, 661 ) 
    func3.SetParameter(4, 40) 
    func3.SetParLimits(0, 0,1)  
    func3.SetParLimits(1, -0.1,0.1) 
    func3.SetParLimits(2, 0.1,0.4)  
    func3.SetParLimits(3, 650,670) 
    func3.SetParLimits(4, 40,42) 
  
    ##assuming that the 2 lines of Co60 have the same intensity
    func4 = TF1("func4","[0]+[1]*x + [2]*TMath::Gaus(x,[3],[4]) + [2]*TMath::Gaus(x,[5],[6])",1000,1600)
    #func4 = TF1("func4","[0]+[1]*x",1000,1600)
    func4.SetParameter(0, 7.43992e-01)  
    func4.SetParameter(1, -9.53915e-04) 
    func4.SetParameter(2, 4.2e-01)
    func4.SetParameter(3, 1173) 
    func4.SetParameter(4, 47) 
    func4.SetParameter(5, 1332) 
    func4.SetParameter(6, 50) 

    func4.SetParLimits(0, 0,1)  
    func4.SetParLimits(1, -1,0) 
    func4.SetParLimits(2, 0.1,0.5)  
    func4.SetParLimits(3, 1100,1200) 
    func4.SetParLimits(4, 46,48) 
    func4.SetParLimits(5, 1250,1380) 
    func4.SetParLimits(6, 50,53) 
    
    spectrum_cal.Fit("func3","LLMR")
    spectrum_cal.Fit("func4","LLMR")
    
    #x2 = [math.sqrt(calibrate(x[0],cal_p0,cal_p1)), math.sqrt(calibrate(x[1],cal_p0,cal_p1)), math.sqrt(calibrate(x[2],cal_p0,cal_p1))]
    #x2 = [math.sqrt(661),math.sqrt(1173),math.sqrt(1332)]
    #y2 = [calibrate(func.GetParameter(4),cal_p0,cal_p1),calibrate(func2.GetParameter(4),cal_p0,cal_p1),calibrate(func2.GetParameter(6),cal_p0,cal_p1)]
    #ey2=[calibrate(func.GetParError(4),cal_p0,cal_p1),calibrate(func2.GetParError(4),cal_p0,cal_p1),calibrate(func2.GetParError(6),cal_p0,cal_p1)]
    
    x2 = [math.sqrt(661),math.sqrt(1173),math.sqrt(1332)]
    ex2 = [0,0,0]
    y2 = [func3.GetParameter(4),func4.GetParameter(4),func4.GetParameter(6)]
    ey2= [func3.GetParError(4),func4.GetParError(4),func4.GetParError(6)]
    
    vx2  = array('d',x2 )
    vex2 = array('d',ex2)
    vy2  = array('d',y2 )
    vey2 = array('d',ey2)
    
    graph2 = TGraphErrors(3,vx2,vy2,vex2,vey2)
    graph2.SetName("resolution")
    line2 = TF1("line2","[0]+[1]*x",25,40)
    line2.SetParameter(0,-0.4)
    line2.SetParameter(1,1.8e-2)
    graph2.Fit("line2","MLLR")
    graph2.GetXaxis().SetTitle("#Sqrt(E) [keV^{1/2}]")
    graph2.GetYaxis().SetTitle("#sigma(E) [keV]")

    print("*******\n Crystal resolution (%f +/- %f) %% / sqrt(E[keV]) \n**************" % (line2.GetParameter(1) * 100, line2.GetParError(1) * 100) )
    print("Crystal sigma(E)  @661 keV =  %f keV  --> FWHM %f  %%" %  (func3.GetParameter(4),  2.35* ( line2.GetParameter(0)+line2.GetParameter(1)*math.sqrt(661.))/661. *100 ))
    print("Crystal sigma(E)  @1173 keV = %f keV --> FWHM %f %%" % ( func4.GetParameter(4),   2.35 *  (line2.GetParameter(0)+line2.GetParameter(1)* math.sqrt(1173.))/1173. * 100))
    print("Crystal sigma(E)  @1332 keV = %f keV --> FWHM %f %%" % ( func4.GetParameter(6),  2.35 * (line2.GetParameter(0)+ line2.GetParameter(1)* math.sqrt(1332.))/1332. * 100 ))
    print("*******\n") 
    
    ###Number of p.e
    #time = 1510 #s
    #h_spectrum_nonorm = h_spectrum.Clone("h_spectrum_nonorm")
    #h_spectrum_nonorm.Scale(time)
    eff = 0.5

    ##Cristallo di Pirro
    #func_pe = TF1("func_pe","[0]+[1]*x + [2]*TMath::Gaus(x,1.6e-10*[3]*[4],TMath::Sqrt(2.56e-20*[3]*[4]*[4] + 0.))", 0.6,2)
    #NaI short
    func_pe = TF1("func_pe","[0]+[1]*x + [2]*TMath::Gaus(x,1.6e-10*[3]*[4],TMath::Sqrt(2.56e-20*[3]*[4]*[4] + 0.048))", 4. ,6.5)
    func_pe.SetParameter(0, func.GetParameter(0))  
    func_pe.SetParameter(1, func.GetParameter(1)) 
    func_pe.SetParameter(2, func.GetParameter(2))  
    func_pe.SetParameter(3, 350) #N p.e
    func_pe.SetParameter(4, 9.2e07) #Gain
    #func_pe.SetParameter(3, 3.25e4) #N p.e
    #func_pe.SetParameter(4, 1e06) #Gain
    #func_pe.SetParameter(3, 8e3) #N p.e
    #func_pe.SetParameter(4, 4e06) #Gain
    
    func_pe.SetParLimits(0, func.GetParameter(0),  func.GetParameter(0))  
    func_pe.SetParLimits(1, func.GetParameter(1),  func.GetParameter(1)) 
    func_pe.SetParLimits(2, func.GetParameter(2),  func.GetParameter(2))  
    func_pe.SetParLimits(3, 2e2,4e2) 
    func_pe.SetParLimits(4, 8e7,1e8) 
    #func_pe.SetParLimits(3,3e4,4e4) 
    #func_pe.SetParLimits(4, 9e5, 2e6) 
    #func_pe.SetParLimits(3,1e3,1e4) 
    #func_pe.SetParLimits(4, 1e6, 5e6) 
 
    h_spectrum.Fit("func_pe","LLMR")

    QE = 0.3
    eff = 1


    print("***********") 
    print("Gain is : (%f +/- %f) "% (func_pe.GetParameter(4),func_pe.GetParError(4)))
    print("Number of p.e  is : (%f +/- %f)  p.e. / keV" %((func_pe.GetParameter(3) / 661.), (func_pe.GetParError(3) / 661.)))
    print("Light yield is : (%f +/- %f) phot. / keV" %((func_pe.GetParameter(3) / QE / eff / 661.), (func_pe.GetParError(3) / QE / eff / 661.)))
    print("***********") 

    c = TCanvas("c","c",700,600)
    c.cd()
    h_spectrum.Draw("")
   

    out_file = TFile(args.output,'recreate')
    out_file.cd()
    c.Write()
    h_spectrum.Write()
    func.Write()
    func2.Write()
    func3.Write()
    func4.Write()
    graph.Write()
    line.Write()
    graph2.Write()
    line2.Write()
    spectrum_cal.Write()
    func_pe.Write()
    out_file.Close()
    #print "N events: %d" % nEvt
    #print "Rate: %f" % (float(nEvt)/float(time))

if __name__ == "__main__":
    main()


#----- keep the GUI alive ------------
if __name__ == '__main__':
  rep = ''
  while not rep in ['q','Q']:
    rep = raw_input('enter "q" to quit: ')
    if 1 < len(rep):
      rep = rep[0]
