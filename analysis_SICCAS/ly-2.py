#!/usr/bin/env python

import matplotlib.pyplot as plt
import math
import numpy as np
import re
import peakutils
import sys

from scipy import optimize
from scipy import signal
from scipy.special import factorial
from numpy import sqrt, pi, exp, linspace, loadtxt

# variable parameters
Egamma = 0.661
filename = "1 inch NaI 1# sample- Cs137.Spe"
if (len(sys.argv) > 1):
    filename = sys.argv[1]

# constants
m      = 0.511                    
Ec     = Egamma/(1.+2.*Egamma/m)

# Gauss: p0 = normalization
#        p1 = mean
#        p2 = sigma
def gauss(p, x):
    N = p[0]
    mu = p[1]
    sigma = p[2]
    return N*exp(-0.5*((x-mu)/sigma)**2)

# read data file
print("====================================================================");
print("Reading data file : " + filename)
print("Egamma            : ", Egamma, " MeV")
print("Backscattering    : ", Ec, " MeV")
print("====================================================================\n");
f = open(filename, 'r')
lines = f.readlines()

xch  = np.arange(1024)
data = np.zeros(1024)

# drop useless rows
i = 0
max = 0
count = 0
for line in lines:
    if line.startswith(' '):
        x = re.sub(" +", "", line);
        x = re.sub("\n", "", x);
        data[i] = math.floor(float(x))
        count += data[i]
        i = i+1
        if (data[i - 1] > count/i*0.05):
            max = i - 1;

# function to fill an array with data
def fill(m, M, data):
    integral = 0.
    xx = np.zeros(M)
    yy = np.zeros(M)
    for i in range(m, M):
        xx[i] = float(i)
        yy[i] = float(data[i])
        integral += yy[i]
    return xx, yy, integral

# prepare arrays of data
xx, yy, integral = fill( 0, max, data)

# find peaks
peakind = peakutils.indexes(yy, thres=.2, min_dist=math.floor(max/5))
nl = len(peakind)
if (nl >= 1):
    print("Peak finding: ", peakind)
    ifp = peakind[nl - 1]
    ic  = peakind[nl - 2]
    ibs = peakind[nl - 3]
else:
    print("Cannot find peaks")
    exit(-1)

# first estimates
print("\n--------------- ESTIMATES --------------------------------------------")
cal = (ifp-ibs)/(Egamma-Ec)
beta = ifp-cal*Egamma
print("Calibration constant : ", cal, " ADC Ch/MeV")
backscatter = Ec*cal
print("Photopeak at         : ", Egamma*cal+beta)
print("Backscatter peak at  : ", backscatter+beta)

# define the ROI
x,  y, integral = fill(ifp-ibs, max, data)

# evaluate some parameters for the fit
fpN = y[ifp]
fpm = ifp
j = ifp + 1
while (y[j] > 0.5*fpN):
    j+=1

fps = (j-ifp)

# fit the photopeak
fp = [y[ifp], fpm, fps]

# define a narrower ROI
xfp,  yfp, integral = fill(fpm-3*fps, fpm+3*fps, data)

errfun = lambda p, x: gauss(p, x) - yfp
fp, success = optimize.leastsq(errfun, fp, args=(xfp))

# fit the backscattering peak
pbs = [y[ibs], ibs, fps]

# define a narrower ROI
xbs,  ybs, integral = fill(math.floor(ibs-0.5*fps), ibs+fps, data)

errfun = lambda p, x: gauss(p, x) - ybs
pbs, success = optimize.leastsq(errfun, pbs, args=(xbs))

# fit results
print(fp)
print(pbs)
ly = (fp[2]**2-pbs[2]**2)/(Egamma - Ec)
print("LY = ", ly, " pe/MeV")
print("----------------------------------------------------------------------\n")

# plot results
plt.plot(xx, yy, marker="+", ls="")
plt.plot(x[peakind], y[peakind], marker="o", ls="", ms=3)
plt.plot(xfp[ifp-3*fps:], gauss(fp, xfp[ifp-3*fps:]))
plt.plot(xbs[ibs-fps:], gauss(pbs, xbs[ibs-fps:]))
plt.xlabel('ADC Counts')
plt.ylabel('Events')
plt.show()


