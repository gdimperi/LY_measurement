#!/usr/bin/env python

import matplotlib.pyplot as plt
import math
import numpy as np
import re
import peakutils
import sys

from scipy import optimize
from scipy import signal
from scipy.special import factorial
from numpy import sqrt, pi, exp, linspace, loadtxt

# variable parameters
filename = "1 inch NaI 1# sample- Cs137.Spe"
if (len(sys.argv) > 1):
    filename = sys.argv[1]

Egamma1 = 0.
Egamma2 = 0.661

# get the source 
na22 = re.search("(N|n)a22", filename)
if na22:
    Egamma1 = 0.511
    Egamma2 = 1.2745

# constants
m      = 0.511                    
Ec1     = Egamma1/(1.+2.*Egamma1/m)
Ec2     = Egamma2/(1.+2.*Egamma2/m)

Egamma = Egamma2
Ec = Ec2

# Gauss: p0 = normalization
#        p1 = mean
#        p2 = sigma
def gauss(x, p0, p1, p2):
    N = p0
    mu = p1
    sigma = p2
    return N*exp(-0.5*((x-mu)/sigma)**2)

# GaussLY: a single gauss LY peak
def gausslyfp(x, C, c, LY, sigma, A, B):
    s = sqrt(sigma**2+c**2*LY*Egamma)
    background = 0.
    if na22:
        background = A + B*x
    return gauss(x, C, c*LY*Egamma, s) + A + B*x

def fermiDirac(x, N, x_F, T):
    return N/(1+exp((x-x_F)/T))

def gausslybs(x, C, c, LY, sigma, NCompton, x_F, T):
    s = sqrt(sigma**2+c**2*LY*Ec)
    return gauss(x, C, c*LY*Ec, s) + fermiDirac(x, NCompton, x_F, T) 

# DoubleGaussLY: double peak
def doubleGaussly(x, C1, C2, c1, c2, LY, sigma1, sigma2, NCompton, x_F, T):
    s1 = sqrt(sigma1**2+c1**2*LY*Ec)
    s2 = sqrt(sigma2**2+c2**2*LY*Egamma)
    return gauss(x, C1, c1*LY*Ec, s1) + fermiDirac(x, NCompton, x_F, T) + gauss(x, C2, c2*LY*Egamma, s2)

# read data file
print("====================================================================");
print("Reading data file : " + filename)
print("Egamma            : ", Egamma, " MeV")
print("Backscattering    : ", Ec, " MeV")
print("====================================================================\n");
f = open(filename, 'r')
lines = f.readlines()

xch  = np.arange(1024)
data = np.zeros(1024)

# drop useless rows
i = 0
max = 0
count = 0
for line in lines:
    if line.startswith(' '):
        x = re.sub(" +", "", line);
        x = re.sub("\n", "", x);
        data[i] = math.floor(float(x))
        count += data[i]
        i = i+1
        if (data[i - 1] > count/i*0.05):
            max = i - 1;


print("Spectrum got up to ", max)

# function to fill an array with data
def fill(m, M, data):
    integral = 0.
    xx = np.zeros(M - m)
    yy = np.zeros(M - m)
    for i in range(0, M - m):
        xx[i] = float(i + m)
        yy[i] = float(data[i + m])
        integral += yy[i]
    return xx, yy, integral

# function to fill an array with sqrt(n)
def errors(y):
    sy = np.zeros(len(y))
    for i in range(0, len(y)):
        sy[i] = sqrt(y[i])
        if (sy[i] == 0):
            sy[i] = 1.
    return sy

# prepare arrays of data
xx, yy, integral = fill( 0, max, data)

# find peaks
peakind = peakutils.indexes(yy, thres = .2, min_dist = math.floor(max/3))
nl = len(peakind)
if (nl >= 1):
    print("Peak finding: ", peakind)
    ifp = peakind[nl - 1]
    ibs = peakind[nl - 2]
else:
    print("Cannot find peaks")
    exit(-1)

def sigmaEstimate(y, p, base):
    j = p
    peakHeight = y[p] - base
    while (y[j] > 0.5*peakHeight + base):
        j+=1
    return j-p

# define the ROI
x,  y, integral = fill(0, max, data)
fpsigma = errors(y)

# first estimates
print("\n--------------- ESTIMATES --------------------------------------------")

def photoPeakFitting():
    print("+----- starting photoPeakFittin() ")
    print("DEBUG: ", ifp, " ", ibs, " ", Egamma, " " , Ec)
    cal = (ifp-ibs)/(Egamma-Ec)
    beta = ifp-cal*Egamma
    print("Calibration constant : ", cal, " ADC Ch/MeV")
    backscatter = Ec*cal
    print("Photopeak at         : ", Egamma*cal+beta)
    print("Backscatter peak at  : ", backscatter+beta)
    
# evaluate some parameters for the fit
    fpN = y[ifp]
    fpm = ifp
    fps = sigmaEstimate(y, ifp, 0)
    
    print("Estimated width      : ", fps)
# define a narrower ROI
    startxfp = fpm - fps
    endxfp = fpm + 3*fps
    print("*** fitting photopeak only from ", startxfp, " to ", endxfp) 
    
# estimate LY
    lyest = (ifp/fps)**2/Egamma
    print("LY estimate          : ", lyest, " pe/MeV")
    c = ifp/(lyest*Egamma)
    print("calibration estimate : ", c, " ch/pe")
    sigma0 = sqrt(fps**2-c**2*lyest*Egamma)
    print("electr. noise est.   : ", sigma0, " ch")
    
# fit the rightmost photopeak
    fp = [y[ifp], c, lyest, sigma0, 0, 0]
    fp, fpcov = optimize.curve_fit(gausslyfp, x[startxfp:endxfp], y[startxfp:endxfp], fp, fpsigma[startxfp:endxfp], True)
    print(fp)
    print("+----- end of photoPeakFittin() ")
    return startxfp, endxfp, fps, fp

startxfp, endxfp, fps, fp = photoPeakFitting()
c = fp[1]
lyest = fp[2]

if na22:
    Egamma = Egamma1
    Ec = Ec1
    ifp = ibs
    ibs = 0
    startxbs, endxbs, fps, fp1 = photoPeakFitting()

electronic_sigma = 10
smearing = 1.1

# ---------------------------------------
# backscattering 
# ---------------------------------------
def backScatteringFitting():
# define a narrower ROI
    startxbs = math.floor(ibs - 2*fps)
    endxbs = math.floor((ifp + ibs)*0.5 + 3*fps)
    print("*** fitting lower energy peak + Compton from ", startxbs, " to ", endxbs) 
    
# estimate background level
    C = y[ibs] - y[startxbs]
    print("Normalization bspeak : ", C)
    print("Compton edge estimate: ", endxbs)
    print("Peak expected at     : ", c*smearing*lyest*Ec)

    baseline = y[math.floor((ifp + ibs)*0.5)]
    sl = fps
    print("Lower peak width     : ", sl)

# fit the backscattering/lower energy peak
    bs = [C, c*smearing, lyest, electronic_sigma, y[startxbs], endxbs, sl]
    bs, fpcov = optimize.curve_fit(gausslybs, x[startxbs:endxbs], y[startxbs:endxbs], bs, fpsigma[startxbs:endxbs], True)
    print(bs)
    return startxbs, endxbs, sl, bs

if not na22:
    startxbs, endxbs, sl, bs = backScatteringFitting() 

# function to extract data in the region of interest
def roi(x, y, x1m, x1M, x2m, x2M):
    l = x1M - x1m + x2M - x2m 
    xx = np.zeros(l)
    yy = np.zeros(l)
    j = 0
    for i in range(0, len(x)):
        if ((x[i] > x1m) and (x[i] <= x1M)):
            xx[j] = x[i]
            yy[j] = y[i]
            j += 1
        if ((x[i] > x2m) and (x[i] <= x2M)):
            xx[j] = x[i]
            yy[j] = y[i]
            j += 1
    return xx, yy

# ---------------------------------------------------------
# whole spectrum
# ---------------------------------------------------------
print("*** Fitting whole spectrum ***")
x2, y2 = roi(x, y, startxbs, endxbs, startxfp, endxfp)
sigma2 = errors(y2)

print("Fit starts at        : ", startxbs)
print("Fit stops at         : ", endxfp)

if not na22:
    print("Compton lev. estimate: ", bs[4])
    print("Compton edge estimate: ", bs[5])
    fullp = [bs[0], fp[0], fp[1]*smearing, fp[1], fp[2], bs[3], fp[3], bs[4], bs[5], bs[6]]
    fullp, fpcov = optimize.curve_fit(doubleGaussly, x2, y2, fullp, sigma2, True)

    
# fit results
    print(fullp)

    cF = fullp[3]
    LYF = fullp[4]
    sF = fullp[6]

    photopeak = cF*LYF*Egamma

# compute chi2
    chi2 = 0.
    for i in range(0, len(y2)):
        chi2 += ((y2[i]-doubleGaussly(x2[i], *fullp))/sigma2[i])**2

    print("Chi2 = ", chi2, " NDF = ", len(y2)-len(fullp)-1, " Chi2_r = ", chi2/(len(y2)-len(fullp)-1))
    print("======================================================================")
    print("LY(fp) = ", fp[2], " pe/MeV")
    print("LY(bs) = ", bs[2], " pe/MeV")
    print("LY     = ", LYF, " pe/MeV c_1 = ", fullp[2], " c_2 = ", fullp[3])
    print("FWHM   = ", 2.35*sqrt(sF**2+cF**2*LYF*Egamma)/photopeak)
    print("======================================================================\n")
    print("----------------------------------------------------------------------\n")

# plot results
plt.errorbar(x, y, yerr=1, xerr=1, marker="o", ls="", ms=2)
plt.errorbar(x2, y2, yerr=sigma2, xerr=1, marker="o", ls="", ms=2)
plt.plot(x[peakind], y[peakind], marker="o", ls="", ms=5)
Egamma = Egamma2
plt.plot(x[startxfp:endxfp], gausslyfp(x[startxfp:endxfp], *fp))
if not na22:
    plt.plot(x[startxbs:endxbs], gausslybs(x[startxbs:endxbs], *bs))
    plt.plot(x[startxbs:], doubleGaussly(x[startxbs:], *fullp))
else:
    Egamma = Egamma1
    plt.plot(x[startxbs:endxbs], gausslyfp(x[startxbs:endxbs], *fp1))


plt.xlabel('ADC Counts')
plt.ylabel('Events')
plt.show()


